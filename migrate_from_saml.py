import json
import psycopg2
import psycopg2.extras

UPDATE_USER_SQL = '''
UPDATE "Users"
SET "accessToken" = NULL, "refreshToken" = NULL,
    "profileid" = %s, "profile" = %s
WHERE id = %s
'''

conn = psycopg2.connect(dbname='codimd', user='<user>', password='<password>',
                        host='dbod-microsvc.cern.ch', port=6602)
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

cur.execute('''
  SELECT
    id, profileid, profile
  FROM "Users"
  WHERE profileid LIKE 'SAML-%'
''')

for user in cur.fetchall():
    profile_id = user['profileid']
    new_profile_id = profile_id.split('SAML-')[1]
    user_profile = json.loads(user['profile'])
    new_user_profile = {
        'id': new_profile_id,
        'username': new_profile_id,
        'displayName': user_profile['username'],
        'provider': 'oauth2',
        'email': user_profile['emails'][0]
    }

    # print(UPDATE_USER_SQL % (new_profile_id, new_user_profile, user['id']))
    cur.execute(UPDATE_USER_SQL, (new_profile_id, json.dumps(new_user_profile), user['id']))

conn.commit()
cur.close()
conn.close()
