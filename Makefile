deploy:
	bash -c "oc create -f <(helm template . --dry-run)"

undeploy:
	bash -c "oc delete -f <(helm template . --dry-run)"
